def nested_length(_ll, tmp=None):
    tmp = [] if tmp is None else tmp
    tmp.append(len(_ll))
    for i in _ll:
        if isinstance(i, list):
            nested_length(i, tmp)
    return sum(tmp)


if __name__ == '__main__':
    ll = [1, 2, 3, [1, 2, [1, 2], [4, [5, [6, [4]]], [3, 1]]]]
    print(nested_length(ll))
