height = int(input('Высота пирамиды: '))
for x in range(1, height + 1):
    for i in range(height * 2 + 1):
        if i == height:
            print('*' * (x * 2 - 1), end="")
            height -= 1
        else:
            print(' ', end="")
    print()
