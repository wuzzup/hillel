import argparse
from datetime import datetime, timedelta


users = {'adm': {'password': "pas", 'last_fail': '2022-07-01 07:35'},
         'user1': {'password': '1994', 'last_fail': '2022-07-01 07:21'}}


class UserDoesNotExist(Exception):
    pass


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--username', type=str, help='Имя пользователя')
    parser.add_argument('-p', '--password', type=str, help='Пароль')
    return parser.parse_args()


def user_block(username, now):
    last_fail = users[username]['last_fail']
    unblock = now - datetime.strptime(last_fail, "%Y-%m-%d %H:%M")
    if unblock < timedelta(minutes=5):
        print("Вы заблокированы!")
        print("Следующая попытка через", timedelta(minutes=5) - unblock, "мин")
        return False
    return True


def check_password_decorator(func):
    def wrapper(username, password):
        try:
            check_password(username, password)
        except UserDoesNotExist as e:
            print(e)
            return False
        if not user_block(username, time):
            return False
        return func(username, password)

    return wrapper


def check_password(username, password):
    if users.get(username) is not None:
        if not users.get(username).get('password') == password:
            raise UserDoesNotExist("Не правильное Имя или Пароль")
    else:
        raise UserDoesNotExist("Не правильное Имя или Пароль")


@check_password_decorator
def login(*args, **kwargs):
    return bool


if __name__ == "__main__":
    time = datetime.now()
    attempt = 3
    while attempt > 0:
        if parse().username is None:
            username = input("Введите Имя: ")
        else:
            username = parse().username

        if parse().password is None:
            password = input("Введите Пароль: ")
        else:
            password = parse().password
        if login(username, password):
            print("Вы в системе!")
            break
        if attempt:
            attempt -= 1
            print("Осталось попыток:", attempt)
            continue

    print('Выйти: exit + Enter.')
    while True:
        s = input('>> ')
        if s == 'exit' or 'e':
            print('Вы вышли.')
        break
