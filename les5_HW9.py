a = [int(i) for i in input('Введите список чисел через пробел:').split()]
count = 0
for i in range(1, len(a) - 1):
    if a[i - 1] < a[i] and a[i] > a[i + 1]:
        count += 1
print('Элементов которые больше соседей: ', count)
# 1 2 5 4 8 6 9 18 10