d_1 = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
d_2 = {'d': 1, 'x': 9, 'c': 6, 'e': 8}

new_dict = d_1.copy()
for key, value in d_1.items():
    for k, v in d_2.items():
        if k not in new_dict.keys() \
                or (k == key and v > value):
            new_dict.update({k: v})
print(new_dict)
