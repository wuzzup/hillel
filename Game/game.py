import random
import sys
from enum import Enum


class Action(Enum):
    Rock = 0
    Paper = 1
    Scissors = 2
    Lizard = 3
    Spock = 4


victories = {
    Action.Scissors: [Action.Lizard, Action.Paper],
    Action.Paper: [Action.Spock, Action.Rock],
    Action.Rock: [Action.Lizard, Action.Scissors],
    Action.Lizard: [Action.Spock, Action.Paper],
    Action.Spock: [Action.Scissors, Action.Rock]
}


def user_choice():
    choices = [f"{action.name}[{action.value}]" for action in Action]
    choices_str = ",".join(choices)
    selection = int(input(f"Enter your choice ({choices_str}): "))
    action = Action(selection)
    return action


def computer_choice():
    selection = random.randint(0, len(Action) - 1)
    action = Action(selection)
    return action


def determine_winner(user_action, computer_action):
    defeats = victories[user_action]
    if user_action == computer_action:
        print(f"Both players selected {user_action.name}. It`s a tie!")
    elif computer_action in defeats:
        print(f"{user_action.name} beats {computer_action.name}! You win!)))")
    else:
        print(f"{computer_action.name} beats {user_action.name}! You lose(((")


if __name__ == "__main__":
    while True:
        try:
            user_action = user_choice()
        except ValueError as e:
            range_str = f"[0, {len(Action) - 1}]"
            print(f"Invalid selection. Enter a value in range {range_str}")
            continue

        computer_action = computer_choice()
        determine_winner(user_action, computer_action)


        play_again = input("If u wanna play again - enter (y)? / For quit press else kay: ")
        if play_again.lower() != "y":
            sys.exit()