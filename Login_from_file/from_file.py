import argparse
import json
from sys import exit
from datetime import datetime, timedelta


class UserDoesNotExist(Exception):
    pass


class WrongPassword(Exception):
    pass


def read_file(file_name):
    try:
        with open(file_name) as f:
            return json.load(f)
    except OSError as e:
        print(e)


def write_file(file_name, data):
    try:
        with open(file_name, 'w') as f:
            json.dump(data, f, ensure_ascii=False)
    except OSError as e:
        print(e)


def registration():
    d = read_file('data.json')
    n = input('Придумайте Имя: ')
    p = input('Придумайте Пароль: ')
    d[n] = {'password': str(p), 'last_fail': "2022-01-01 00:00"}
    write_file('data.json', d)


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--username', help=' Your username')
    parser.add_argument('--password', help='Your password')
    return parser.parse_args()


def check_password(username, password):
    users = read_file('data.json')
    if users.get(username) is not None:
        if not users.get(username).get("password") == password:
            users[username]["last_fail"] = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M")
            write_file('data.json', users)
            raise WrongPassword("Неверный Пароль")
    else:
        raise UserDoesNotExist("Пользователь не найден")


def user_block(username):
    users = read_file('data.json')
    try:
        un_b = datetime.now() - datetime.strptime(users[username]["last_fail"], "%Y-%m-%d %H:%M")
        if un_b < timedelta(minutes=5):
            print(f"Вы заблокированы! Следующая попытка через {timedelta(minutes=5) - un_b} мин")
            exit()
    except KeyError:
        return False


def decor(func):
    def wrapper(*args, **kwargs):
        try:
            check_password(username, password)

        except UserDoesNotExist:
            print('Зарегистрироваться? (yes/no):')
            answer = input()
            if answer == "yes":
                registration()
                return True
            elif answer == "no":
                pass
                return False

        except WrongPassword as e:
            print(e)
            return False

        if not user_block(username):
            return False
        return func(*args, **kwargs)

    return wrapper


@decor
def login(username, password):
    return True


if __name__ == "__main__":
    attempt = 3
    while attempt > 0:
        if parse().username is None:
            username = input("Введите Имя: ")
        else:
            username = parse().username
        if parse().password is None:
            password = input("Введите Пароль: ")
        else:
            password = parse().password
        if login(username, password):
            print("Вы в системе!")
            break
        if attempt:
            attempt -= 1
            print("Осталось попыток:", attempt)
        else:
            print("Нет попыток.")


    print('Выйти: exit + Enter.')
    while True:
        s = input('>> ')
        if s == 'exit' or 'e':
            print('Вы вышли.')
            break